
'use strict';

/******************
	Module Imports
 ******************/

require('dotenv').config({silent: true});

var
	// bluebird promises
	Promise = require('bluebird'),

	// user authentication strategies
	passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,

	// user sessions and session storage
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),

	// cryptography
	bcrypt = require('bcrypt'),
	crypto = require('crypto'),

	// utilities
	bodyParser = require('body-parser');

/**********************
	Server Configuration
 **********************/

// run the server
var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 3000, function () {
    console.log("Server running on port: %j", server.address().port);
});

/***********************
 	Database Confiuration
 ***********************/

// connect to the database
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/source-mart');
mongoose.Promise = Promise;
var db = mongoose.connection;

// error connecting to the databse
db.on('error', function () {
    console.error("Could not connect to the database");
});

// connected successfully to the database
db.once('open', function () {
    console.log("Database running");
});

/**********************
	Passport Middleware
 **********************/

// serialize user
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

// deserialize user
passport.deserializeUser(function (id, done) {
    User.findOne({ _id: id }, function (err, user) {
        done(err, user);
    });
});

// configure passport signup strategy
passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, next) {

    process.nextTick(function () {
        User.findOne({ email: email }, function (err, user) {

            if (err) return next(err, null);

            if (user) return next(null, false, {
                status: "Error",
                message: 'Email is already registered, please log in'
            });

            return next(null, req.body);

        });
    });

}));

// configure passport login strategy
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function (email, password, done) {

    User.findOne({
        email: email
    }, function (err, user) {

		console.log("Logging in", email, password);

        // if there was an error finding the user
        if (err) return done(err);

        // if the user is not registered
        if (!user) {
            return done(null, false, {
                status: "Error",
                message: 'Email is not registered'
            });
        }

        // if the user is registered but the password is wrong
        user.isValidPassword(password, function (err, isMatch) {
            if (!isMatch) return done(null, false, {
                status: "Error",
                message: 'Incorrect password'
            });
            return done(null, user);
        });

    });
}));

/*******************
	Other Middleware
 *******************/

// allow CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});

// serve static content
app.use(express.static(__dirname + '/public'));

// // parse incoming requests
// app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// sessions
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'amdsourcemart',
    store: new MongoStore({ mongooseConnection: db })
}));

// authentication
app.use(passport.initialize());
app.use(passport.session());

function sendError (err, res) {
	return res.send({
		status: "Error",
		message: err.message,
		error: err.errors
	});
}

function sendUnAuth (res) {
	return res.send({
		status: "Error",
		message: "You are not authorised to do that"
	});
}
