var Schema = mongoose.Schema;

// data types
var ObjectId = Schema.Types.ObjectId;
var Mixed = Schema.Types.Mixed;

// regex
var nameRegex = [/^([a-zA-Z '])+$/, "Your name may only contain alphabetical characters and an apostraphe"];
var emailRegex =[ /@/, "That is not a valid email"];
var passwordRegex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[_\W]).{8,20}$/;
