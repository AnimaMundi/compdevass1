
// define the snippet schema
var SnippetSchema = new Schema({
	_author: {
		type: ObjectId,
		required: "Snippet user ID is required",
		ref: "User"
	},
	title: {
		type: String,
		required: "Snippet title is required"
	},
	editor_theme: {
		type: String,
		required: "Snippet editor theme is required"
	},
	editor_mode: {
		type: String,
		required: "Snippet editor mode is required"
	},
	source: {
		type: String,
		required: "Snippet source code is required"
	}
});

// initialize the snippet model
var Snippet = mongoose.model('Snippet', SnippetSchema);

// add a new snippet to the database
function addNewSnippet (snippet) {
	return Snippet.create(snippet);
}

// get all snippets in the database
function getAllSnippets () {
	return querySnippets({});
}

// get all snippets by a particular User
function getSnippetsByUser (user_id) {
	return querySnippets({ user_id: user_id });
}

// get all snippets in a particular category
function getSnippetsByCategory (category) {
	return querySnippets({ editor_mode: category });
}

// get a single snippet from the database
function getSnippetById (id) {
	return Snippet.findById(id).populate('_author').exec();
}

// delete a specific snippet from the database
function deleteSnippet (id) {
	return Snippet.findByIdAndRemove(id).exec();
}

// update a specific snippet in the database
function updateSnippet (id, snippetData) {
	return Snippet.findById(id).exec()
	.then(function (snippet) {
		snippet.title = snippetData.title;
		snippet.editor_theme = snippetData.editor_theme;
		snippet.editor_mode = snippetData.editor_mode;
		snippet.source = snippetData.source;
		return snippet.save();
	});
}

// query the snippet collection
function querySnippets (query) {
	return Snippet.find(query).populate('_author').exec();
}

// query for on snippet from the colleciton
function queryOneSnippet (query) {
	return Snippet.findOne(query).populate('_author').exec();
}

// remove snippets from the collection
function removeSnippets (query) {
	return Snippet.remove(query).exec();
}

// update snippets in the collection
function updateSnippets (query, data) {
	return Snippet.update(query, data).exec();
}
