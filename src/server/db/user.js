
// define the user schema
var UserSchema = new Schema({
    first_name: {
        type: String,
        match: nameRegex,
        required: true
    },
    last_name: {
        type: String,
        match: nameRegex,
        required: true
    },
    email: {
        type: String,
        required: true,
		match: emailRegex,
        index: {
            unique: true
        }
    },
	password: {
		type: String,
		required: true
	},
    created: {
        type: Date,
        default: Date.now
    }
});

// encrypt the user's password on save
UserSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });

});

// remove a user's data in other tables when they delete their account
UserSchema.pre('remove', function (next) {
	Snippet.remove({ _author: this._id }, function (err) {
		if (err) return next(err);
		return next();
	});
});

// encrypt a given password and check the cipher text against the encrypted password stored
UserSchema.methods.isValidPassword = function(candidatePassword, next) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return next(err);
        next(null, isMatch);
    });
};

// define the User model using the User Schema
var User = mongoose.model('User', UserSchema);

// add a new user to the database
function addNewUser (user) {
    return User.create(user);
}

// delete a specific user from the database
function deleteUser  (id) {
	return User.findById(id).exec()
	.then(function (user) {
		return user.remove();
	});
}

function updateUser (id, userData) {
	return User.findById(id).exec()
	.then(function (user) {
		user.first_name = userData.first_name;
		user.last_name = userData.last_name;
		user.email = userData.email;
		return user.save();
	});
}
