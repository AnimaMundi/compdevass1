//signup a new customer
app.post('/auth/signup', function (req, res, next) {

    passport.authenticate('local-signup', function (err, user, info) {

        // if there was an error authenticating the user
        var error = err || info;
        if (error) return res.send({
			status: "Error",
			message: error.message,
			error: error.errors
		});

		addNewUser(user)
		.then(function (user) {

			// if the user was authenticated, log in the user
			req.logIn(user, function (err) {

				if (err) return res.send({
					status: "Error",
					message: err.message,
					error: err.errors
				});

				// set cookie expiry date
				var expiration = 100*365*24*60*60*1000;
				if (req.user.remember_me) req.session.cookie.maxAge = expiration;
				else req.session.cookie.maxAge = null;

				res.send({
					status: "Success",
					message: "User signed up successfully",
					user: req.user
				});
			});

		}).catch(function (err) {
			return res.send({
				status: "Error",
                message: err.message,
                error: err.errors
            });
		});

    })(req, res, next);
});

// log a user in
app.post('/auth/login', function (req, res, next) {

    // set cookie expiry date
    var expiration = 100*365*24*60*60*1000;
    if (req.body.remember_me) req.session.cookie.maxAge = expiration;
    else req.session.cookie.maxAge = null;

    passport.authenticate('local', function (err, user, info) {

        // if there was an error authenticating the user
        var error = err || info;
        if (error) return res.send({
			status: "Error",
			message: error.message,
			error: error.error
		});

        // if the user was authenticated, log in the user
        req.logIn(user, function (err) {

            if (err) return res.send({
				status: "Error",
				message: err.message,
				error: err.error
			});

            return res.send({
                status: "Success",
				message: "User logged in successfully",
                user: req.user
            });
        });

    })(req, res, next);
});

// log a user out, if they are already logged in
app.post('/auth/logout', function (req, res) {

    // if the user is logged in
    if (req.user) {

        req.logout();
		return res.send({
			status: "Success",
			message: "User logged out successfully"
		});

    }

	// if the user is not logged in
	return res.send({
		status: "Error",
		message: "There is no user logged in"
	});

});

// get session info
app.get('/auth/session', function (req, res) {

    if (req.session && req.session.passport && req.session.passport.user && req.session.passport.user) {
        return res.send({
            status: "Success",
            message: "User is logged in",
            user: req.user
        });
    }

    return res.send({
        status: "Error",
        message: "No user logged in"
    });

});

app.route('/api/users/:id')

// update a user
.put(function (req, res) {
    updateUser(req.params.id, req.body)
	.then(function (user) {
		return res.send({
			status: "Success",
			message: "User updated successfully",
			user: user
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
})

// delete a user
.delete(function (req, res) {
	deleteUser(req.params.id)
	.then(function () {
		return res.send({
			status: "Success",
			message: "User deleted successfully"
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
});
