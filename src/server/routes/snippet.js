app.route('/api/snippets')

// get all snippets from the database
.get(function (req, res) {
	getAllSnippets()
	.then(function (snippets) {
		return res.send({
			status: "Success",
			message: "Snippets retireved successfully",
			snippets: snippets
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
})

// add a new snippet to the database
.post(function (req, res) {
	addNewSnippet(req.body)
	.then(function (snippet) {
		return res.send({
			status: "Success",
			message: "Snippet created successfully",
			snippet: snippet
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
});

app.get('/api/snippets/category/:category', function (req, res) {
	getSnippetsByCategory(req.params.category)
	.then(function (snippets) {
		return res.send({
			status: "Success",
			message: "Snippets retrieved successfully",
			snippets: snippets
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
});

app.route('/api/snippets/:id')

// get a specific snippet from the database
.get(function (req, res) {
	getSnippetById(req.params.id)
	.then(function (snippet) {
		return res.send({
			status: "Success",
			message: "Snippet retireved successfully",
			snippet: snippet
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
})

// update a specific snippet in the database
.put(function (req, res) {
	updateSnippet(req.params.id, req.body)
	.then(function () {
		return res.send({
			status: "Success",
			message: "Snippet updated successfully"
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
})

// delete a specific snippet from teh database
.delete(function (req, res) {
	deleteSnippet(req.params.id)
	.then(function () {
		return res.send({
			status: "Success",
			message: "Snippet deleted successfully"
		});
	}).catch(function (err) {
		return sendError(err, res);
	});
});
