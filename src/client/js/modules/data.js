var data = angular.module('App.Data', []);

data.service('AuthService', ['$rootScope', '$modal', '$state', '$http', function ($rootScope, $modal, $state, $http) {

	var self = this;

	// log the user in
	this.login = function (credentials) {
		return $http.post('/auth/login', credentials);
	};

	// log the user out
    this.logout = function () {
        return $http.post('/auth/logout');
    };

	// signup a new user
    this.signup = function (credentials) {
        return $http.post('/auth/signup', credentials);
    };

	// get the current user's session
	this.getSession = function () {
		return $http.get('/auth/session');
	};

	// delete the user's account
	this.deleteAccount = function (password) {
		return $http.delete('/api/users/' + $rootScope.user._id);
	};

	// update the user's account
	this.updateAccount = function (user) {
		return $http.put('/api/users/' + $rootScope.user._id, user);
	};

	// open the signup modal
	this.openSignupModal = function (next) {

		// open the modal
		$modal.open({
			animation: true,
			templateUrl: 'modals/signup-modal.html',

			// signup controller
			controller: ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

				// signup a user
				$scope.signup = function (user) {

					return self.signup(user)
					.success(function (res) {

						if (res.status == "Error") {
							console.error("Couldn't sign user up", res);
							return $rootScope.$emit('errMsg', "Could not sign up user", res.error);
						} else {
							console.log("User signed up", res);
							return self.login({
								email: user.email,
								password: user.password
							});
						}

					}).success(function (res) {
						if (res.status == "Error") {
							console.error("Couldn't log user in", res);
							return $rootScope.$emit('errMsg', "Could not sign up user", res.error);
						} else {
							console.log("User loged in", res);
							$scope.$dismiss();
							$rootScope.user = res.user;
							return $rootScope.$emit('showAlert', {
								type: 'success',
								message: 'Account created successfully'
							});
						}
					}).error(function (err) {
						console.error("Couldn't sign user up", res);
						$rootScope.$emit('errMsg', "Could not sign up user", res.error);
					});
				};

			}]
		});
	};

	// open the login modal
    this.openLoginModal = function (next) {

        // open the modal
        $modal.open({
            animation: true,
            templateUrl: 'modals/login-modal.html',

            // modal controller
            controller: ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

                // login the user
                $scope.login = function (credentials) {
                    self.login(credentials)
					.success(function (res) {
						if (res.status == "Error") {
							console.error("Couldn't log user in", res);
							$rootScope.$emit('showAlert', {
								type: 'danger',
								message: res.message
							});
						} else {
							console.log("User loged in", res);
							$scope.$dismiss();
							$rootScope.user = res.user;
							$rootScope.$emit('showAlert', {
								type: 'success',
								message: 'Account created successfully'
							});
						}
					}).error(function (err) {
						console.error("Couldn't log user in", err);
						$rootScope.$emit('showAlert', {
							type: 'danger',
							message: err.message
						});
					});

                };

            }]
        });
    };

}]);


data.service('SnippetsDataService', ['$http', '$rootScope', function ($http, $rootScope) {

	var root = "/api/snippets/";

	// retrieve all snippets from the database
    this.getAll = function () {
		return new Promise(function (resolve, reject) {
			$http.get(root)
			.success(function (res) {
				console.log("Succss", res);
				$rootScope.$emit('showAlert', {
					type: "success",
					message: "Snippets retrieved successfully"
				});
				console.log("Snippets", res.snippets);
				resolve(res.snippets);
			}).error(function (err) {
				console.error("Error", err);
				$rootScope.$emit('showAlert', {
					type: "danger",
					message: "Error retrieving snippets\n" + res.message
				});
				reject();
			});
		});
    };

	// add a new snippet to the database
    this.addNew = function (snippet) {
		return new Promise(function (resolve, reject) {
			$http.post(root, snippet)
			.success(function (res) {
				console.log("Success", res);
				if (res.status == "Error") {
					$rootScope.$emit('errMsg', "Could not save snippet", res.error);
				} else {
					$rootScope.$emit('showAlert', {
						type: "success",
						message: "Snippet saved successfully"
					});
					resolve(res.snippet);
				}
			}).error(function (err) {
				console.error("Error", err);
				$rootScope.$emit('showAlert', {
	                type: "error",
	                message: "Snippet could not be added\n" + err.message
	            });
				reject();
			});
		});
    };

	// remove a snippet from the database
    this.remove = function (snippet) {
		return new Promise(function (resolve, reject) {
			$http.delete(root + snippet._id)
			.success(function (res) {
				console.log("Success", res);
				$rootScope.$emit('showAlert', {
	                type: "success",
	                message: "Snippet removed successfully"
	            });
				resolve();
			}).error(function (err) {
				console.error("Error", err);
				$rootScope.$emit('showAlert', {
	                type: "error",
	                message: "Snippet could not be removed\n" + err
	            });
				reject();
			});
		});
    };

	// update a specific snippet in the database
    this.update = function (snippet) {
		return new Promise(function (resolve, reject) {
			$http.put(root + snippet._id, snippet)
			.success(function (res) {
				console.log("Success", res);

				if (res.status == "Error") {
					$rootScope.$emit('errMsg', "Could not update snippet", res.error);
				} else {
					$rootScope.$emit('showAlert', {
		                type: "success",
		                message: "Snippet saved successfully"
		            });
					resolve(res.snippet);
				}

			}).error(function (err) {
				console.error("Error", res);
				$rootScope.$emit('showAlert', {
					type: "error",
					message: "Snippet could not be saved\n" + err
				});
				reject();
			});
		});
    };

	// retrieve a specific snippet from the database
    this.getById = function (id) {
        return new Promise(function (resolve, reject) {
			$http.get(root + id)
			.success(function (res) {
				console.log("Success", res);
				$rootScope.$emit('showAlert', {
	                type: "success",
	                message: "Snippet retrieved successfully"
	            });
				resolve(res.snippet);
			}).error(function (err) {
				console.error("Error", err);
				$rootScope.$emit('showAlert', {
					type: "error",
					message: "Snippet could not be retrieved\n" + err
				});
				reject();
			});
		});
    };

    this.getByCategory = function (category) {
		return new Promise(function (resolve, reject) {
			$http.get(root + 'category/' + category)
			.success(function (res) {
				console.log("Got Snippets", res);
				$rootScope.$emit('showAlert', {
					type: "success",
					message: "Snippets retrieved successfully"
				});
				resolve(res.snippets);
			}).error(function (err) {
				console.error("Error", err);
				$rootScope.$emit('showAlert', {
					type: "error",
					message: "Snippets could not be retrieved\n" + err
				});
				reject();
			});
		});
    };

}]);
