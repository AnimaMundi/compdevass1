var directives = angular.module('App.Directives', []);

directives.directive('aceEditor', ['$document', function ($document) {
    return {
        restrict: 'E',
        scope: {
            snippet: '=',
            aceLoaded: '=',
            aceChanged: '=',
            save: '='
        },
        templateUrl: 'directives/ace-editor.html',
        link: function ($scope, element, attributes) {

            console.log("Scope", $scope);

            // this caused a bug where old snippets from a different user were being saved again
            // under the current user
            // $document.bind('keypress', function(e) {
            //     var key = e.which || e.keyCode;
            //     if (!e.ctrlKey) return;
            //     if (key == 115) {
            //         e.preventDefault();
            //         $scope.save();
            //     }
            // });

            var capitilize = function (str) {
                var strArr = str.split('_');
                var output = '';
                var length = strArr.length;
                for (var i = 0; i < length; i++) {
                    var currentWord = strArr[i];
                    var capitalLettter = currentWord.charAt(0).toUpperCase();
                    currentWord = capitalLettter + currentWord.slice(1, currentWord.length);
                    if (output.length) output += ' ';
                    output += currentWord;
                }
                return output;
            };

            var capitilizeMeta = function () {
                if (!$scope.snippet.editor_mode || !$scope.snippet.editor_theme) return;
                if ($scope.snippet.editor_mode == 'javascript') $scope.editor_mode = 'Javascript';
                else if ($scope.snippet.editor_mode == 'html') $scope.editor_mode = 'HTML';
                else if ($scope.snippet.editor_mode == 'css') $scope.editor_mode = 'CSS';
                $scope.editor_theme = capitilize($scope.snippet.editor_theme).replace(' ', '_');
            };

			$scope.$watch('snippet', function () {
				if (!$scope.snippet) return;
				capitilizeMeta();
			});

            $scope.$watch('editor_mode', function () {
                if (!$scope.editor_mode) return;
                $scope.snippet.editor_mode = $scope.editor_mode.toLowerCase();
            });

            $scope.$watch('editor_theme', function () {
                if (!$scope.editor_theme) return;
                $scope.snippet.editor_theme = $scope.editor_theme.toLowerCase().replace(' ', '_');
            });

            $scope.aceModes = [
                'Javascript',
                'HTML',
                'CSS'
            ];

            $scope.aceThemes = [
                'Ambiance',
                'Chaos',
                'Chrome',
                'Clouds',
                'Clouds Midnight',
                'Cobalt',
                'Crimson Editor',
                'Dawn',
                'Dreamweaver',
                'Eclipse',
                'Github',
                'Idle Fingers',
                'iPlastic',
                'Katzenmilch',
                'KR Theme',
                'Kuroir',
                'Merbivore',
                'Merbivore Soft',
                'Mono Industrial',
                'Monokai',
                'Pastel on Dark',
                'Solarized Dark',
                'Solarized Light',
                'SQL Server',
                'Terminal',
                'Text Mate',
                'Tomorrow Night',
                'Tomorrow Night Blue',
                'Tomorrow Night Bright',
                'Tomorrow Night Eighties',
                'Twilight',
                'Vibrant Ink',
                'xCode'
            ];

        }
    };
}]);
