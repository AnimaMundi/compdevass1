var snippet = angular.module('App.Snippet', []);

snippet.controller('SnippetsIndexCtrl', ['$scope', 'SnippetsDataService', function ($scope, SnippetsDataService) {

}]);

snippet.controller('AddSnippetCtrl', ['$scope', '$rootScope', '$state', 'SnippetsDataService', function ($scope, $rootScope, $state, SnippetsDataService) {

	$scope.snippet = {};

    $scope.save = function () {
        $scope.snippet._author = $rootScope.user._id;
		SnippetsDataService.addNew($scope.snippet)
		.then(function (snippet) {
	        $state.go('site.edit-snippet', { id: snippet._id });
		});
    };

    $scope.aceLoaded = function(_editor) {
        console.log("Ace Loaded");
        _editor.setReadOnly(false);
    };

    $scope.aceChanged = function(e) {
        console.log("Ace changed");
    };

}]);

snippet.controller('EditSnippetCtrl', ['$scope', 'SnippetsDataService', '$stateParams', '$state', function ($scope, SnippetsDataService, $stateParams, $state) {

	SnippetsDataService.getById($stateParams.id)
    .then(function (snippet) {
		$scope.snippet = snippet;
	});

    $scope.onAceLoaded = function(_editor) {
        console.log("Ace Loaded");
        _editor.setReadOnly(false);
    };

    $scope.onAceChanged = function(e) {
        console.log("Ace Loaded");
    };

    $scope.save = function () {
        SnippetsDataService.update($scope.snippet);
    };

    $scope.remove = function () {
        SnippetsDataService.remove($scope.snippet)
		.then(function () {
			$state.reload();
			$state.go('site.home');
		});
    };

}]);

snippet.controller('ViewSnippetCtrl', ['$scope', 'SnippetsDataService', '$stateParams', function ($scope, SnippetsDataService, $stateParams) {

	SnippetsDataService.getById($stateParams.id)
	.then(function (snippet) {
		$scope.snippet = snippet;
	});


    $scope.onAceLoaded = function(_editor) {
        console.log("Ace Loaded");
        _editor.setReadOnly(true);
    };

    $scope.onConAceChangedhange = function(e) {
        console.log("Ace Loaded");
    };

}]);
