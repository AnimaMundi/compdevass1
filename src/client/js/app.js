var app = angular.module('App', [

    // core angular modules
    'ngAnimate', 'ngSanitize',

    // angular ui modules
	'ui.bootstrap', 'ui.router', 'ui.ace',

    // application specific modules
	'App.Views', 'App.Data', 'App.Directives',

    // application models
    'App.Snippet'
]);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider

    .state('site', {
        url: '',
        templateUrl: 'elements/site-wrapper.html',
        controller: 'AppCtrl'
    })

    .state('site.home', {
        url: '/home',
        templateUrl: 'pages/landing-page.html'
    })

    .state('site.account', {
        url: '/account',
        templateUrl: 'pages/account.html',
        controller: 'AccountCtrl'
    })

	.state('site.snippets-index', {
		url: '/snippets',
		templateUrl: 'pages/snippets/snippets-index.html',
		controller: 'SnippetsIndexCtrl'
	})

    .state('site.add-snippet', {
        url: '/add-snippet',
        templateUrl: 'pages/snippets/add-snippet.html',
        controller: 'AddSnippetCtrl'
    })

    .state('site.edit-snippet', {
        url: '/edit-snippet/:id',
        templateUrl: 'pages/snippets/edit-snippet.html',
        controller: 'EditSnippetCtrl'
    })

    .state('site.view-snippet', {
        url: '/view-snippet/:id',
        templateUrl: 'pages/snippets/view-snippet.html',
        controller: 'ViewSnippetCtrl'
    });

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.when('/', '/home');

    $locationProvider.html5Mode(true);

}]);

app.run(['$rootScope', 'AuthService', function ($rootScope, AuthService) {
	AuthService.getSession()
	.success(function (res) {
		console.log("Session", res);
		$rootScope.user = res.user;
	}).error(function (err) {
		console.error("Error getting session", err);
	});
}]);

app.controller('AppCtrl', ['$scope', '$rootScope', 'AuthService', 'SnippetsDataService', '$timeout', function ($scope, $rootScope, AuthService, SnippetsDataService, $timeout) {

	$scope.snippets = [];

    $scope.signup = function () {
        AuthService.openSignupModal();
    };

    $scope.login = function () {
        AuthService.openLoginModal();
    };

    $scope.logout = function () {
        AuthService.logout()
		.success(function (res) {
			$rootScope.user = null;
		}).error(function (err) {
			$rootScope.$emit('showAlert', {
				type: 'danger',
				message: "Could not log out\n" + res.message
			});
		});
    };

	SnippetsDataService.getAll()
	.then(function (snippets) {
		$scope.snippets = snippets;
	});

    $scope.refine = {
        keywords: '',
        order: '_id',
        direction: false
    };

	$scope.selectedCategory = 'all';
	$scope.goToCategory = function (category) {
		$scope.selectedCategory = category;
		if (category == 'all') SnippetsDataService.getAll().then(function (snippets) {
			$scope.snippets = snippets;
		});
		else SnippetsDataService.getByCategory(category).then(function (snippets) {
			 $scope.snippets = snippets;
		});
	};

    var alertTimeout;

    $scope.alert = {
        type: "success",
        message: "Hello",
        visible: false
    };

    $rootScope.$on('showAlert', function ($event, alert) {
        $scope.alert = alert;
        $scope.alert.visible = true;
        alertTimeout = $timeout(function () {
            $scope.alert.visible = false;
        }, 3000);
    });

    $scope.closeAlert = function () {
        $scope.alert.visible = false;
        $timeout.cancel(alertTimeout);
    };

	$rootScope.$on('errMsg', function ($event, message, errors) {
		console.log("Building error message", message, errors);
		var errMsg = "";
		for (var key in errors) {
			var err = errors[key];
			errMsg += "<br><br>" + err.name + ":<br>" + err.path + " - " + err.message;
		}

		// display the alert
		$scope.alert = {
			type: "danger",
			message: message + errMsg,
			visible: true
		};

	});

}]);

app.controller('AccountCtrl', ['$scope', '$rootScope', 'AuthService', '$state', function ($scope, $rootScope, AuthService, $state) {

    $scope.deleteAccount = function () {
        AuthService.deleteAccount()
		.success(function (res) {
			console.log("Success", res);
			$rootScope.$emit('showAlert', {
				type: 'success',
				message: "Removed account successfully"
			});
			$state.reload();
			$state.go('site.home');
			$rootScope.user = null;
		}).error(function (err) {
			console.error("Error", err);
			$rootScope.$emit('showAlert', {
				type: 'danger',
				message: "Could not remove account\n" + res.message
			});
		});
    };

	$scope.updateAccount = function (user) {
		AuthService.updateAccount(user)
		.success(function (res) {
			console.log("Success", res);
			if (res.status == "Error") {
				$rootScope.$emit('errMsg', "Could not update account", res.error);
			} else {
				$rootScope.$emit('showAlert', {
					type: 'success',
					message: "Updated account successfully"
				});
				$rootScope.user = res.user;
			}

		}).error(function (err) {
			console.error("Error", err);
			$rootScope.$emit('showAlert', {
				type: 'danger',
				message: "Could not update account\n" + err.message
			});
		});
	};

}]);
