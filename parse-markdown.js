var fs = require('fs'),
	md = require('marked');

md.setOptions({
	renderer: new md.Renderer(),
	gfm: true,
	tables: true,
	breaks: true,
	pedantic: false,
	sanitize: true,
	smartLists: true,
	smartypants: true
});

var parseHtml = function () {

	fs.readFile('README.md', 'utf-8', function (err, data) {

		if (err) {
			process.exit(1);
			console.error("There was an error reading the markdown file", err);
		}

		console.log("Markdown file read successfully");

		fs.writeFile('README.html', md(data), 'utf-8', function (err) {

			if (err) {
				process.exit(1);
				return console.error("There was an error writing the HTML file", err);
			}

			console.log("HTML file wrote successfully");
		});
	});

};

parseHtml();

console.log("Watching file README.md for changes");

fs.watchFile('README.md', function (curr, prev) {
	
	console.log("Changes detected");
	
	parseHtml();
	
});
