'use strict';

// libraries & plugins
var del = require('del'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rework = require('gulp-rework'),
    nodemon = require('gulp-nodemon'),
    reworkNPM = require('rework-npm'),
    gulpFilter = require('gulp-filter'),
    bowerSrc = require('gulp-bower-src'),
    runSequence = require('run-sequence'),
    minifyCss = require('gulp-minify-css'),
    templateCache = require('gulp-angular-templatecache');

var path = {
    dev: 'builds/development/',
    prod: 'builds/production/',
    root: 'src/',
    server: 'server/',
    client: 'client/'
};

var src = {
    server: path.root + path.server,
    client: path.root + path.client
};

var serverJsSrc = [
    src.server + 'index.js',
    src.server + 'routes/**/*.js',
    src.server + 'routes.js',
    src.server + 'db/db.js',
    src.server + 'db/**/*.js'
];

var clientJsSrc = [
    src.client + 'js/app.js',
    src.client + 'js/views.js',
    src.client + 'js/modules/**/*.js'
];

gulp.task('default', ['run', 'watch']);

var filter = gulpFilter('**/*.js');

gulp.task('bower', function (done) {
    bowerSrc()
        .pipe(filter)
        .pipe(gulp.dest('./dist/lib'))
        .pipe(concat('lib.js'))
        .pipe(gulp.dest(path.dev + 'public'))
        .pipe(uglify())
        .pipe(gulp.dest(path.prod + 'public'));
});

gulp.task('watch', function () {
    gulp.watch(serverJsSrc, ['server']);
    gulp.watch(clientJsSrc, ['client']);
    gulp.watch(src.client + 'html/**/*.html', ['html']);
    gulp.watch(src.client + 'sass/**/*.scss', ['sass']);
    gulp.watch(src.client + 'css/**/*.css', ['css']);
});

gulp.task('build', function (done) {
    runSequence(['build-dep', 'build-client', 'server'], done);
});

gulp.task('build-dep', function (done) {
    runSequence(['bower', 'sass', 'css'], done);
});

gulp.task('build-client', function (done) {
    runSequence('html', 'client', done);
});

gulp.task('sass', function () {
    gulp.src(src.client + 'sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(src.client + 'css'));
});

gulp.task('css', function () {

    // clean up
    del(path.dev + 'public/app.css');
    del(path.prod + 'public/app.css');

    // rework and minify css
    gulp.src(src.client + 'css/**/*.css')
        .pipe(concat('app.css'))
        .pipe(rework(reworkNPM()))
        .pipe(gulp.dest(path.dev + 'public/'))
        .pipe(minifyCss())
        .pipe(gulp.dest(path.prod + 'public/'));
});

gulp.task('html', function (done) {
    runSequence('cleanup-html', 'concat-html', done);
});

// clean up all html
gulp.task('cleanup-html', function () {
    del(src.client + 'js/modules/views.js');

    del(path.prod + 'public/index.html');
    gulp.src(src.client + 'html/index.html')
        .pipe(gulp.dest(path.prod + 'public/'));

    del(path.dev + 'public/index.html');
    gulp.src(src.client + 'html/index.html')
        .pipe(gulp.dest(path.dev + 'public/'));
});

// concatenate public html
gulp.task('concat-html', function () {
    return gulp.src(src.client + 'html/views/**/*.html')
        .pipe(templateCache('views.js', {
        module: 'App.Views',
        standalone: true
    }))
        .pipe(gulp.dest(src.client + 'js/modules/'));
});

gulp.task('client', function () {

    // clean up
    del(path.dev + 'public/app.js');
    del(path.prod + 'public/app.js');

    // concat client-side js
    gulp.src(clientJsSrc)
        .pipe(concat('app.js'))
        .pipe(gulp.dest(path.dev + 'public/'))
        .pipe(uglify())
        .pipe(gulp.dest(path.prod + 'public/'));
});

gulp.task('server', function () {

    // clean up
    del(path.dev + 'index.js');
    del(path.prod + 'index.js');

    // concat server-side js
    gulp.src(serverJsSrc)
        .pipe(concat('index.js'))
        .pipe(gulp.dest(path.dev))
        .pipe(uglify())
        .pipe(gulp.dest(path.prod));
});

gulp.task('run', function () {
    nodemon({
        script: path.dev + 'index.js',
        ext: 'js',
        env: {
            'NODE_ENV': 'development'
        }
    });
});
