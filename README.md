# Component Development

### Assignment 1 - Client Side SPA - Proposal
### Nathan Mc Grath - 20065681
### Applied Computing - Games Development Stream

## Features Implemented

### Views

- Landing Page
- Account Page
- Snippets
    - Add New
    - Edit Existing
    - View Snippet
- Elements
    - Header
    - Site-Wrapper
- Directives
    - Ace Editor

### Entities

- User
- Snippet

### Services

- Firebase Data Service
- Auth Service
- User Data Service
- Snippet Data Service

### Misc. Features

- ngAnimate, animate.css & sassy css powered animations
- Firebase Integration
    - User Registration and authentication
    - Data persistence
- Categorisation of scope data
- ngRepeat data filtering and ordering
- Ace Editor Integration
- Sass Bootstrap Customisation
- Live Deployment available at http://source-mart.eu, if DNS has not yet propagated then deployment can be found at http://46.101.4.66:4020

## Proposed Web Application Concept

The concept that I would like to implement for my first assignment with relation to Component Development is that of an e-commerce site.
Products that will be available for purchase from the site will be code snippets uploaded by the sites user&#39;s.
Users will be able to both sell & purchase code snippets, projects & templates.

When browsing code snippets, the actual code itself will not be visible, but it's result will be visible in the browser where possible.
Where this is not possible, particularly for server side code, tests may be supplied in order to verify the functionality of the code.
Seller profiles that have high ratings will appear before profiles with lower ratings, and likewise for individual code snippets.

Payments will processed by Stripe, whom provide both browser and server side libraries.
In order to sell code, users will need to upgrade their account to a seller account which involves creating a Stripe account and giving the site access to their Stripe account.
A small checklist will ensure that everything is in order before allowing code snippets to be made public and sellable, this includes:
- Email Verification through Stripe.
- Identity Verification through Stripe.
- Having a &quot;Live&quot;, not a &quot;Test&quot; account on Stripe.

## Web Application Features

- User authentication, signup & login
- Seller profile & code reviews, ratings & comments
- Code categorisation
  - HTML
  - CSS
    - General
	- Library & Framework Specific
    - Preprocessor (SASS & LESS)
  - JavaScript
    - General
    - Library & Framework specific
- Project bootstrapping using yeoman
- Code Mirror Integration
- Dynamic iFrame embedding of code written in-browser to be run in-browser
- Parse Backend Service

## Data Model

![Entity Relationship Diagram](entity_relationship_diagram.jpg "Entity Relationship Diagram")

## Folder Structure

```javascript
root/
|-->.git/
|-->bower_components/
|-->builds
|   |-->development/
|   |   |-->public/
|   |   |   |-->fonts/
|   |   |   |-->img/
|   |   |   |-->app.css
|   |   |   |-->app.js
|   |   |   |-->favicon.ico
|   |   |   |-->index.html
|   |   |   |-->lib.js
|   |   |-->index.js
|   |-->production/
|       |-->.git/
|       |-->public/
|       |   |-->fonts/
|       |   |-->img/
|       |   |-->app.css
|       |   |-->app.js
|       |   |-->favicon.ico
|       |   |-->index.html
|       |   |-->lib.js
|       |-->index.js
|-->docs/
|-->node_modules/
|-->src/
|   |-->client/
|   |   |-->css/
|   |   |-->html/
|   |   |   |-->views/
|   |   |   |-->index.html
|   |   |-->js/
|   |   |   |-->modules/
|   |   |   |-->app.js
|   |   |-->sass/
|   |-->server/
|       |-->db/
|       |-->routes/
|       |-->index.js
|-->.htaccess
|-->bower.json
|-->gulpfile.js
|-->package.json
|-->parse-markdown.js
|-->README.html
|-->README.md
```
